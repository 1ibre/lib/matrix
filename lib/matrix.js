
class Matrix {

    /**
     * Contruct a matrix from size (fill with zeros)
     * @param {number} cols 
     * @param {number} rows 
     */
    constructor(rows, cols) {
        this.rows = [];
        for (let i = 0; i < rows; i++) {
            this.rows.push([]);
            for (let j = 0; j < cols; j++) {
                this.rows[i].push(0);
            }
        }
    }


    /**
     * Construct a matrix from a nested array
     * @param {Array} data 
     * @returns the matrix
     */
    static from(data) {
        let m = new Matrix(data.length, data[0].length);
        // Deep copy from data
        for (let i = 0; i < data.length; i++) {
            for (let j = 0; j < data[0].length; j++) {
                m.rows[i][j] = data[i][j];
            }
        }
        return m;
    }

    static identity(rows, cols) {
        let id = new Matrix(rows, cols);
        for (let i = 0; i < rows; i++) {
            for (let j = 0; j < cols; j++) {
                id.rows[i][j] = i == j ? 1 : 0;
            }
        }
        return id;
    }

    /**
     * Get shape of the matrix
     * @returns the number of rows and columns of the matrix
     */
    get size() {
        return [this.rows.length, this.rows.length > 0 ? this.rows[0].length : 0];
    }

    /**
     * Deep copy the matrix
     * @returns a copy of the matrix
     */
    copy() {
        let cp = new Matrix(this.rows.length, this.rows[0].length);
        // Deep copy the matrix
        for (let i = 0; i < this.rows.length; i++) {
            for (let j = 0; j < this.rows[i].length; j++) {
                cp.rows[i][j] = this.rows[i][j];
            }
        }
        return cp;
    }

    /**
     * Get matrix component
     * @param {number} i 
     * @param {number} j 
     * @returns Matrix component at given indices
     */
    get(i, j) {
        return this.rows[i][j];
    }


    /**
     * Compute matrix addition
     * @param {Matrix} other 
     * @returns Matrix sum
     */
    add(other) {
        let result = this.copy();
        if (this.rows.length !== other.rows.length || this.rows[0].length !== other.rows[0].length) {
            throw new Error('Algae Error: Matrix dimensions must match. Cannot add matrices.');
        } else {
            for (let i = 0; i < this.rows.length; i++) {
                for (let j = 0; j < this.rows[0].length; j++) {
                    result.rows[i][j] += other.rows[i][j];
                }
            }
            return result;
        }
    }

    /**
     * Compute matrix substraction
     * @param {Matrix} other
     * @returns Matrix difference
     */
    sub(other) {
        let result = this.copy();
        if (this.rows.length !== other.rows.length || this.rows[0].length !== other.rows[0].length) {
            throw new Error('Algae Error: Matrix dimensions must match. Cannot substract matrices.');
        } else {
            for (let i = 0; i < this.rows.length; i++) {
                for (let j = 0; j < this.rows[0].length; j++) {
                    result.rows[i][j] -= other.rows[i][j];
                }
            }
        }
        return result;
    }

    /**
     * Compute matrix multiplication
     * @param {Matrix} other 
     * @returns Matrix product
     */
    mul(other) {
        // Check if matrix multiplication is possible
        if (this.rows[0].length !== other.rows.length) {
            throw new Error('Algae Error: Matrix dimensions do not match. Cannot multiply matrices.');
        } else {
            let result = this.copy();
            // Perform matrix multiplication
            for (let i = 0; i < result.rows.length; i++) {
                for (let j = 0; j < result.rows[0].length; j++) {
                    result.rows[i][j] = 0;
                    for (let k = 0; k < this.rows[0].length; k++) {
                        result.rows[i][j] += this.rows[i][k] * other.rows[k][j];
                    }
                }
            }
            return result;
        }
    }

    /**
     * Compute matrix division
     * @param {Matrix} other
     * @return Matrix division
     */
    div(other) {
        return this.mul(other.inv());
    }

    /**
     * Scale the matrix by a scalar
     * @param {number} scalar
     * @returns the scaled matrix
     */
    scale(scalar) {
        let result = this.copy();
        for (let i = 0; i < result.rows.length; i++) {
            for (let j = 0; j < result.rows[0].length; j++) {
                result.rows[i][j] *= scalar;
            }
        }
        return result;
    }

    /** Transpose the matrix 
     * @returns the transposed matrix 
     */
    transpose() {
        let result = this.copy();
        for (let i = 0; i < result.rows.length; i++) {
            for (let j = 0; j < result.rows[0].length; j++) {
                result.rows[i][j] = this.rows[j][i];
            }
        }
        return result;
    }

    /**
     * Get transposed matrix (shortcut)
     * @returns the transposed matrix
     */
    get T() {
        return this.transpose();
    }

    /**
     * Compute the inverse of the matrix
     * @returns the inverse of the matrix
     */
    inv() {
        let inv = this.copy();
        // if (inv.det() == 0) {
        //     throw new Error('Algae Error: Matrix is not invertible. Detirminant is zero.');
        // }
        // Declare variables
		let ratio;
		let a;
		let n = inv.rows.length;

		// Put an identity matrix to the right of matrix
		inv.cols = 2 * n;
		for (let i = 0; i < n; i++) {
			for (let j = n; j < 2 * n; j++) {
				if (i === (j - n)) {
					inv.rows[i][j] = 1;
				}
				else {
					inv.rows[i][j] = 0;
				}
			}
		}

		for (let i = 0; i < n; i++) {
			for (let j = 0; j < n; j++) {
				if (i !== j) {
					ratio = inv.rows[j][i] / inv.rows[i][i];
					for (let k = 0; k < 2 * n; k++) {
						inv.rows[j][k] -= ratio * inv.rows[i][k];
					}
				}
			}
		}

		for (let i = 0; i < n; i++) {
			a = inv.rows[i][i];
			for (let j = 0; j < 2 * n; j++) {
				inv.rows[i][j] /= a;
			}
		}

		// Rmove the left-hand identity matrix
		for (let i = 0; i < n; i++) {
			inv.rows[i].splice(0, n);
		}
		return inv;
    }

    /** 
     * Check if matrix is squared
     * @returns true if matrix is squared, false otherwise
     */
    isSquared() {
        return this.rows.length === this.rows[0].length;
    }

    /** 
     * Check if matrix is triangular
     * @returns true if matrix is triangular, false otherwise
     */
    isTriangular() {
        throw new Error('Algae Error: Not implemented.');
    }

    /**
     * Check if matricies are equal
     * @param {Matrix} other
     * @returns true if matricies are equal, false otherwise
     */
    equals(other) {
        if (this.rows.length !== other.rows.length || this.rows[0].length !== other.rows[0].length) {
            return false;
        }
        for (let i = 0; i < this.rows.length; i++) {
            for (let j = 0; j < this.rows[0].length; j++) {
                if (this.rows[i][j] !== other.rows[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }


    /** Compute determinant 
     * @returns the determinant of the matrix 
     */
    det() {
        let result = this.copy();
        let [rows, cols] = this.size;
        if (rows !== cols) {
            throw new Error('Algae Error: Matrix is not square. Cannot compute determinant.');
        }
        let h = 0;
        let k = 0;
        while (h < rows && k < cols) {
            let i_max = h;
            for (let i = h + 1; i < rows; i++) {
                if (Math.abs(result.rows[i][k]) > Math.abs(result.rows[i_max][k])) {
                    i_max = i;
                }
            }
            if (result.rows[i_max][k] === 0) {
                k++;
            } else {
                let f = result.rows[i_max][k];
                for (let i = h; i < rows; i++) {
                    let factor = result.rows[i][k] / f;
                    for (let j = k + 1; j < cols; j++) {
                        result.rows[i][j] -= result.rows[h][j] * factor;
                    }
                    result.rows[i][k] = 0;
                }
                h++;
                k++;
            }
        }
        let det = 1;
        for (let i = 0; i < rows; i++) {
            det *= result.rows[i][i];
        }
        return det;
    }

    /**
     * Get matrix as string
     * @returns matrix representation as a string
     */
    toString() {
        let string = "[";
        for (let i = 0; i < this.rows.length; i++) {
            let row = this.rows[i];
            string += "[";
            for (let j = 0; j < row.length - 1; j++) {
                string += `${row[j]}, `;
            }
            if (row.length > 0) {
                string += `${row[row.length - 1]}`;
            }
            string += "]"
            if (i < this.rows.length - 1) {
                string += ', ';
                string += "\n";
            }
        }
        string += "]";
        return string
    }
}

module.exports = Matrix;